# PGRF úkol 2

## Ovládání
### S
Náhodně vygeneruje dva pravidelné polygony, na další kliknutí je možné spusti scan line vyplnění.

### W
Přepne se do módu úpravy polygonů. Tažením levého tlačítka myši poblíž bodu je možné bod posouvat, při držení levého shiftu je při tažení vytvořen nový bod. Pravím tlačítkem je možné bod smazat.  

### F
Přepne do módu vyplenění pomocí seed fill algoritmu. Stisknutím levého tlačítka myši vybere bod od kterého bude oblast vyplňována.

### L
Přepne do módu vykreslení pravoúhlého n úhelníku

### R
Smaže vše vykreslené

### C
Mód kreslení čáry

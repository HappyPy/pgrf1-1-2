package cz.wexder.pgrf.domain;

import static cz.wexder.pgrf.constants.Constants.WIDTH;

public class Line {
    private PointDto starterPoint;
    private PointDto endPoint;

    public Line(PointDto starterPoint, PointDto endPoint) {
        this.starterPoint = starterPoint;
        this.endPoint = endPoint;
        orient();
    }

    public IntersectionDto getIntersecting(int currentY) {
        double a1 = endPoint.getY() - starterPoint.getY();
        double b1 = starterPoint.getX() - endPoint.getX();
        double c1 = a1 * starterPoint.getX() + b1 * starterPoint.getY();

        PointDto intersecingStart = new PointDto(0, currentY);
        PointDto intersecingEnd = new PointDto(WIDTH, currentY);

        double a2 = intersecingEnd.getY() - intersecingStart.getY();
        double b2 = intersecingStart.getX() - intersecingEnd.getX();
        double c2 = a2 * intersecingStart.getX() + b2 * intersecingStart.getY();

        double delta = a1 * b2 - a2 * b1;
        double x = (b2 * c1 - b1 * c2) / delta;
        double y = (a1 * c2 - a2 * c1) / delta;

        return new IntersectionDto(new PointDto((int) Math.round(x), (int) Math.round(y)), currentY >= starterPoint.getY() && currentY < endPoint.getY());
    }

    private void orient() {
        if (starterPoint.getY() > endPoint.getY()) {
            PointDto temp = endPoint;
            endPoint = starterPoint;
            starterPoint = temp;
        }
    }

}

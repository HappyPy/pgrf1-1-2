package cz.wexder.pgrf.domain;

public class IntersectionDto {
    private PointDto point;
    private boolean intersecting;

    public IntersectionDto(PointDto point, boolean intersecting) {
        this.point = point;
        this.intersecting = intersecting;
    }

    public PointDto getPoint() {
        return point;
    }

    public void setPoint(PointDto point) {
        this.point = point;
    }

    public boolean isIntersecting() {
        return intersecting;
    }

    public void setIntersecting(boolean intersecting) {
        this.intersecting = intersecting;
    }
}

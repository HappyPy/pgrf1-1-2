package cz.wexder.pgrf.domain;

import java.util.ArrayList;

public class RegularPolygon {

    private Polygon polygon;

    public RegularPolygon(PointDto point, float alpha, float radius, int n) {
        polygon = new Polygon();
        draw(point, alpha, radius, n);
    }

    public void draw(PointDto poin, float alpha, float radius, int n) {
        polygon.setPoints(new ArrayList<>());
        if (n < 3){
            n = 3;
        }
        if (n >50){
            n = 50;
        }
        for (int i = 0; i < n; i++) {
            int x = (int)(poin.getX() + radius * Math.cos(alpha + i * 2 * Math.PI / n));
            int y = (int)(poin.getY() + radius * Math.sin(alpha + i * 2 * Math.PI / n));
            polygon.addPoint(new PointDto(x, y));
        }
    }


    public Polygon getPolygon() {
        return polygon;
    }
}

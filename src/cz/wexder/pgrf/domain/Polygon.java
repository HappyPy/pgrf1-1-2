package cz.wexder.pgrf.domain;

import cz.wexder.pgrf.renderer.LineRenderer;

import java.util.ArrayList;
import java.util.List;

public class Polygon {
    List<PointDto> points;

    public Polygon() {
        points = new ArrayList<>();
    }

    public void addPoint(PointDto pointDto) {
        points.add(pointDto);
    }

    public List<PointDto> getPoints() {
        return points;
    }

    public void setPoints(List<PointDto> points) {
        this.points = points;
    }

    public void draw(LineRenderer lineRenderer, int color) {
        Integer lastX = null, lastY = null;
        for (PointDto point : points) {
            if (lastX == null && lastY == null) {
                lastX = point.getX();
                lastY = point.getY();
            } else {
                lineRenderer.drawLine(lastX, lastY, point.getX(), point.getY(), color);
                lastX = point.getX();
                lastY = point.getY();
            }
        }
        if (points.size() > 0) {
            lineRenderer.drawLine(points.get(0).getX(), points.get(0).getY(), points.get(points.size() - 1).getX(), points.get(points.size() - 1).getY());
        }
    }

    public Polygon cut(Polygon cutter) {
        Polygon newPolygon = new Polygon();
        newPolygon.setPoints(getPoints());
        int len = cutter.getPoints().size();
        for (int i = 0; i < len; i++) {

            int len2 = newPolygon.getPoints().size();
            List<PointDto> input = newPolygon.getPoints();
            newPolygon = new Polygon();

            PointDto cutterStart = cutter.getPoints().get((i + len - 1) % len);
            PointDto cutterEnd = cutter.getPoints().get(i);

            for (int j = 0; j < len2; j++) {
                PointDto inputStart = input.get((j + len2 - 1) % len2);
                PointDto inputEnd = input.get(j);

                if (isInside(cutterStart, cutterEnd, inputEnd)) {
                    if (!isInside(cutterStart, cutterEnd, inputStart)){
                        newPolygon.addPoint(getIntersecting(cutterStart, cutterEnd, inputStart, inputEnd));
                    }
                    newPolygon.addPoint(inputEnd);
                } else if (isInside(cutterStart, cutterEnd, inputStart)){
                    newPolygon.addPoint(getIntersecting(cutterStart, cutterEnd, inputStart, inputEnd));
                }
            }
        }
        return newPolygon;
    }

    private boolean isInside(PointDto a, PointDto b, PointDto c) {
        return (a.getX() - c.getX()) * (b.getY() - c.getY()) > (a.getY() - c.getY()) * (b.getX() - c.getX());
    }

    private PointDto getIntersecting(PointDto starterPoint, PointDto endPoint, PointDto intersecingStart, PointDto intersecingEnd) {
        double a1 = endPoint.getY() - starterPoint.getY();
        double b1 = starterPoint.getX() - endPoint.getX();
        double c1 = a1 * starterPoint.getX() + b1 * starterPoint.getY();

        double a2 = intersecingEnd.getY() - intersecingStart.getY();
        double b2 = intersecingStart.getX() - intersecingEnd.getX();
        double c2 = a2 * intersecingStart.getX() + b2 * intersecingStart.getY();

        double delta = a1 * b2 - a2 * b1;
        double x = (b2 * c1 - b1 * c2) / delta;
        double y = (a1 * c2 - a2 * c1) / delta;

        return new PointDto((int) Math.round(x), (int) Math.round(y));
    }

    @Override
    public String toString() {
        StringBuilder out = new StringBuilder();
        for (PointDto point : points) {
            out.append(String.format("Point: %s:%s", point.getX(), point.getY()));
            out.append("\n");
        }
        return out.toString();
    }
}

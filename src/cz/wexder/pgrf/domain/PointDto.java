package cz.wexder.pgrf.domain;

public class PointDto {
    private int x;
    private int y;
    private int color;

    public PointDto(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public PointDto(int x, int y, int color) {
        this.x = x;
        this.y = y;
        this.color = color;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }


    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "PointDto{" +
                "x=" + x +
                ", y=" + y +
                ", color=" + color +
                '}';
    }
}

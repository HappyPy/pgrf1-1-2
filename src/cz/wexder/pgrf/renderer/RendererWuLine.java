package cz.wexder.pgrf.renderer;

import cz.wexder.pgrf.raster.Rastr;

import java.awt.*;

import static java.lang.Math.*;

public class RendererWuLine extends LineRenderer {

    public RendererWuLine(Rastr rastr) {
        setImg(rastr);
    }

    int ipart(double x) {
        return (int) x;
    }

    double fpart(double x) {
        return x - floor(x);
    }

    double rfpart(double x) {
        return 1.0 - fpart(x);
    }

    public void drawLine(int x0, int y0, int x1, int y1) {

        boolean steep = Math.abs(y1-y0)> Math.abs(x1-x0);
        double gradient;

        if(steep){
            int c = y0;
            y0 = x0;
            x0 = c;
            c = x1;
            x1 = y1;
            y1 = c;
        }
        if(x1 < x0) {
            int d = x1;
            x1 = x0;
            x0 = d;
            d = y1;
            y1 = y0;
            y0 = d;
        }

        double dx = x1 - x0;
        double dy = y1 - y0;

        if (dx == 0.0){
            gradient =1;
        }else {
            gradient = dy / dx;
        }

        int xEnd = round(x0);
        int yEnd = (int)(y0+gradient*(xEnd-x0));
        int xGap = (int) rfpart(x0+0.5);

        int xPxl1 = xEnd;
        int yPxl1 = (int)ipart(yEnd);

        if (steep) {
            rastr.drawPixel(yPxl1, xPxl1, 0.5);
            rastr.drawPixel((yPxl1 + 1), xPxl1, 0.5);
        } else {
            rastr.drawPixel(xPxl1, yPxl1, 0.5);
            rastr.drawPixel(xPxl1, yPxl1 + 1, 0.5);
        }

        double intery = yEnd + gradient;

        //second end point
        xEnd = round(x1);
        yEnd = (int)(y1+gradient*(xEnd-x1));
        xGap = (int) rfpart(x1+2);

        int xPxl2 = xEnd;
        int yPxl2 = ipart(yEnd);

        if (steep) {
            rastr.drawPixel(yPxl2, xPxl2, rfpart(yEnd)*xGap);
            rastr.drawPixel((yPxl2 + 1), xPxl2, fpart(yEnd)*xGap);
        } else {
            rastr.drawPixel(xPxl2, yPxl2, rfpart(yEnd) * xGap);
            rastr.drawPixel(xPxl2, yPxl2 + 1, fpart(yEnd) * xGap);
        }

        if(steep){
            for(int i = xPxl1+1; i<=xPxl2-1; i++){
                rastr.drawPixel(ipart(intery),i,rfpart(intery));
                rastr.drawPixel(ipart(intery)+1,i,fpart(intery));
                intery +=gradient;
            }
        }else {
            for(int i = xPxl1+1; i<=xPxl2-1; i++){
                rastr.drawPixel(i,ipart(intery),rfpart(intery));
                rastr.drawPixel(i,ipart(intery)+1,fpart(intery));
                intery +=gradient;
            }
        }
    }

    @Override
    public void drawLine(int x0, int y0, int x1, int y1, int lineColor) {
        rastr.setColor(lineColor);
        drawLine(x0,y0,x1,y1);
    }
}

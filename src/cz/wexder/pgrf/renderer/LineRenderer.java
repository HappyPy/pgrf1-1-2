package cz.wexder.pgrf.renderer;

import cz.wexder.pgrf.raster.Rastr;


public abstract class LineRenderer {

    protected Rastr rastr;
    public void setImg(Rastr rastr) {
        this.rastr = rastr;
    }

    public abstract void drawLine(int x0, int y0, int x1, int y1);

    public abstract void drawLine(int x0, int y0, int x1, int y1, int lineColor);

}

package cz.wexder.pgrf.filler;

import cz.wexder.pgrf.domain.IntersectionDto;
import cz.wexder.pgrf.domain.Line;
import cz.wexder.pgrf.domain.PointDto;
import cz.wexder.pgrf.domain.Polygon;
import cz.wexder.pgrf.raster.Rastr;

import java.util.*;

import static cz.wexder.pgrf.constants.Constants.HEIGHT;
import static cz.wexder.pgrf.constants.Constants.WIDTH;


public class ScanLineFiller implements Filler {
    private Polygon polygon;
    private Rastr img;
    private List<PointDto> points;

    public ScanLineFiller(Rastr img) {
        this.img = img;
        points = new ArrayList<>();
    }

    @Override
    public void fill(int color) {
        fill(new int[][]{
                {color},
        });
    }

    public void fill(int[][] pattern) {
        if (polygon != null) {
            if (polygon.getPoints().size() > 0) {
                int fmax = getFMax().getY();
                int fmin = getFMin().getY();
                List<Line> lines = getLines();
                List<List<PointDto>> intersections = createIntersections(lines, fmin, fmax);
                for (List<PointDto> list : intersections) {
                    PointDto[] array = new PointDto[list.size()];
                    bubbleSort(list.toArray(array));
                    for (int i = 0; i < array.length - 1; i += 2) {
                        for (int x = array[i].getX(); x < array[i + 1].getX(); x++) {
                            if (x >= 0 && array[i].getY() >= 0 && x < WIDTH && array[i].getY() < HEIGHT) {
                                int paternY = array[i].getY() % pattern.length;
                                int paternX = x % pattern[paternY].length;
                                points.add(new PointDto(x, array[i].getY(), pattern[paternY][paternX]));
                            }
                        }
                    }
                }
            }
        }
    }

    private void bubbleSort(PointDto[] array) {
        int n = array.length;
        PointDto temp;
        for (int i = 0; i < n; i++) {
            for (int j = 1; j < (n - i); j++) {
                if (array[j - 1].getX() > array[j].getX()) {
                    temp = array[j - 1];
                    array[j - 1] = array[j];
                    array[j] = temp;
                }
            }
        }

    }

    private List<List<PointDto>> createIntersections(List<Line> lines, int fmin, int fmax) {
        List<List<PointDto>> intersections = new ArrayList<>();
        for (int y = fmin; y <= fmax; y++) {
            intersections.add(new ArrayList<>());
            for (Line line : lines) {
                IntersectionDto intersection = line.getIntersecting(y);
                if (intersection.isIntersecting()) {
                    intersections.get(intersections.size() - 1).add(intersection.getPoint());
                }
            }
        }
        return intersections;
    }

    private PointDto getFMin() {
        PointDto pointDto = polygon.getPoints().get(0);

        for (PointDto point : polygon.getPoints()) {
            if (pointDto.getY() > point.getY()) {
                pointDto = point;
            }
        }

        return pointDto;
    }

    private PointDto getFMax() {
        PointDto pointDto = polygon.getPoints().get(0);
        for (PointDto point : polygon.getPoints()) {
            if (pointDto.getY() < point.getY()) {
                pointDto = point;
            }
        }
        return pointDto;
    }

    private List<Line> getLines() {
        PointDto last = null;
        List<Line> lines = new ArrayList<>();
        List<PointDto> points = polygon.getPoints();
        for (PointDto point : points) {
            if (last == null) {
                last = point;
            } else {
                if (last.getY() != point.getY()) {
                    lines.add(new Line(last, point));
                }
                last = point;
            }
        }
        if (points.size() > 0) {
            if (points.get(0).getY() != points.get(points.size() - 1).getY()) {
                lines.add(new Line(points.get(0), points.get(points.size() - 1)));
            }
        }
        return lines;
    }

    @Override
    public void drawFilled() {
        for (PointDto point : points) {
            img.drawPixel(point.getX(), point.getY(), point.getColor());
        }
    }

    @Override
    public void clearFilled() {
        points.clear();
    }

    public void setPolygon(Polygon polygon) {
        this.polygon = polygon;
    }

}

package cz.wexder.pgrf.filler;

import cz.wexder.pgrf.domain.PointDto;
import cz.wexder.pgrf.raster.Rastr;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import static cz.wexder.pgrf.constants.Constants.HEIGHT;
import static cz.wexder.pgrf.constants.Constants.WIDTH;

public class SeedFillBuffer implements Filler {
    private Rastr img;
    private int backgroundColor;
    private List<PointDto> points;
    private PointDto point;
    private boolean[][] filled = new boolean[WIDTH][HEIGHT];

    public SeedFillBuffer(Rastr img) {
        this.img = img;
        points = new ArrayList<>();
    }

    @Override
    public void fill(int color) {
        fill(new int[][]{{color}});
    }

    @Override
    public void fill(int[][] pattern) {
        if(img != null && point != null){
            backgroundColor = img.getPixelColor(point.getX(), point.getY());
            Queue<PointDto> queue = new LinkedList<>();
            queue.add(point);

            while (!queue.isEmpty()) {
                PointDto p = queue.remove();
                int x = p.getX();
                int y = p.getY();
                if (canFill(img, x, y)) {
                    filled[x][y] = true;
                    int paternY = y % pattern.length;
                    int paternX = x % pattern[paternY].length;
                    points.add(new PointDto(x, y, pattern[paternY][paternX]));
                    queue.add(new PointDto(x, y - 1));
                    queue.add(new PointDto(x, y + 1));
                    queue.add(new PointDto(x - 1, y));
                    queue.add(new PointDto(x + 1, y));
                }
            }
        }
    }

    private boolean canFill(Rastr img, int x, int y) {
        if (!(x >= 0 && y >= 0 && x < WIDTH && y < HEIGHT)) return false;

        if (filled[x][y]) return false;

        return img.getPixelColor(x, y) == backgroundColor;
    }

    @Override
    public void drawFilled() {
        for (PointDto point : points) {
            img.drawPixel(point.getX(), point.getY(), point.getColor());
        }
    }

    @Override
    public void clearFilled() {
        points.clear();
        filled = new boolean[WIDTH][HEIGHT];
    }

    public void setPoint(PointDto point) {
        this.point = point;
    }
}

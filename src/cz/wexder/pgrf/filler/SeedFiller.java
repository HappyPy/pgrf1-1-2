package cz.wexder.pgrf.filler;

import cz.wexder.pgrf.domain.PointDto;
import cz.wexder.pgrf.raster.Rastr;

import java.util.ArrayList;
import java.util.List;

import static cz.wexder.pgrf.constants.Constants.HEIGHT;
import static cz.wexder.pgrf.constants.Constants.WIDTH;

@Deprecated
public class SeedFiller implements Filler {
    private Rastr img;
    private int backgroundColor;
    private List<PointDto> points;
    private boolean[][] filled = new boolean[WIDTH][HEIGHT];
    private PointDto point;

    public SeedFiller(Rastr img) {
        this.img = img;
        points = new ArrayList<>();
    }


    @Override
    public void fill(int color) {
        backgroundColor = img.getPixelColor(point.getX(), point.getY());
        seedFill(point, new int[][]{{color}});
    }

    @Override
    public void fill(int[][] pattern) {
        seedFill(point,pattern);
    }

    private void seedFill(PointDto point, int[][] pattern) {
        int x = point.getX();
        int y = point.getY();
        if (x >= 0 && y >= 0 && x < WIDTH && y < HEIGHT) {
            if (img.getPixelColor(x, y) == backgroundColor && !filled[x][y]) {
                int paternY = y % pattern.length;
                int paternX = x % pattern[paternY].length;
                points.add(new PointDto(x, y, pattern[paternY][paternX]));
                filled[x][y] = true;
                seedFill(new PointDto(x + 1, y), pattern);
                seedFill(new PointDto(x - 1, y), pattern);
                seedFill(new PointDto(x, y + 1), pattern);
                seedFill(new PointDto(x, y - 1), pattern);
            }
        }
    }

    @Override
    public void drawFilled() {
        for (PointDto point : points) {
            img.drawPixel(point.getX(), point.getY(), point.getColor());
        }
    }

    @Override
    public void clearFilled() {
        points.clear();
        filled = new boolean[WIDTH][HEIGHT];
    }

    public void setPoint(PointDto point) {
        this.point = point;
    }
}

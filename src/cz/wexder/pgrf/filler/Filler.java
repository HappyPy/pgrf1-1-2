package cz.wexder.pgrf.filler;

public interface Filler {
    public void fill(int color);

    public void fill(int[][] pattern);

    public void drawFilled();

    public void clearFilled();
}

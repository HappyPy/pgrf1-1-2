package cz.wexder.pgrf.constants;

public class Constants {

    public static int WIDTH = 1000;
    public static int HEIGHT = 600;

    public static final int LINE_MODE = 1;
    public static final int LINE_CONTINUOUS_MODE = 2;
    public static final int REGULAR_POLYGON_MODE = 3;
    public static final int DRAWING_REGULAR_POLYGON_MODE = 4;
    public static final int SEED_FILL_MODE = 5;
    public static final int SCAN_FILL_MODE = 6;
    public static final int MOVE_MODE = 7;
}

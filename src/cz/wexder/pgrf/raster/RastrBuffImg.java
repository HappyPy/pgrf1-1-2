package cz.wexder.pgrf.raster;

import java.awt.*;
import java.awt.image.BufferedImage;

import static cz.wexder.pgrf.constants.Constants.HEIGHT;
import static cz.wexder.pgrf.constants.Constants.WIDTH;

public class RastrBuffImg implements Rastr {

    private BufferedImage img;

    private int color = Color.WHITE.getRGB();

    private int backGroundColor = 0x2f2f2f;

    public void setColor(int color) {
        this.color = color;
    }

    public RastrBuffImg(BufferedImage img) {
        this.img = img;
    }

    @Override
    public void drawPixel(int x, int y) {
        drawPixel(x, y, 1f);
    }

    @Override
    public void drawPixel(double x, double y, double c) {
        double alfaChanel = Math.round(c * 100.0) / 100.0;
        int drawColor = color;

        if (alfaChanel >= 0 && alfaChanel <= 0.2) {
            drawColor = blend(backGroundColor, color, (float) alfaChanel);
        } else if (alfaChanel > 0.2 && alfaChanel <= 0.4) {
            drawColor = blend(backGroundColor, color, (float) alfaChanel);
        } else if (alfaChanel > 0.4 && alfaChanel <= 0.6) {
            drawColor = blend(backGroundColor, color, (float) alfaChanel);
        } else if (alfaChanel > 0.6 && alfaChanel <= 0.8) {
            drawColor = blend(backGroundColor, color, (float) alfaChanel);
        } else if (alfaChanel > 0.8 && alfaChanel <= 1.0) {
            drawColor = blend(backGroundColor, color, (float) alfaChanel);
        }
        if (x >= 0 && y >= 0 && x < WIDTH && y < HEIGHT) {
            img.setRGB((int) x, (int) y, drawColor);
        }
    }

    @Override
    public void drawPixel(int x, int y, int color) {
        if (x >= 0 && y >= 0 && x < WIDTH && y < HEIGHT) {
            img.setRGB(x, y, color);
        }
    }

    @Override
    public int getPixelColor(int x, int y){
        Color mycolor = new Color(img.getRGB(x, y));
        return mycolor.getRGB() & 0xffffff;
    }

    private int blend(int a, int b, float ratio) {
        if (ratio > 1f) {
            ratio = 1f;
        } else if (ratio < 0f) {
            ratio = 0f;
        }

        float iRatio = 1.0f - ratio;

        int aA = (a >> 24 & 0xff);
        int aR = ((a & 0xff0000) >> 16);
        int aG = ((a & 0xff00) >> 8);
        int aB = (a & 0xff);

        int bA = (b >> 24 & 0xff);
        int bR = ((b & 0xff0000) >> 16);
        int bG = ((b & 0xff00) >> 8);
        int bB = (b & 0xff);

        int A = (int) ((aA * iRatio) + (bA * ratio));
        int R = (int) ((aR * iRatio) + (bR * ratio));
        int G = (int) ((aG * iRatio) + (bG * ratio));
        int B = (int) ((aB * iRatio) + (bB * ratio));

        return A << 24 | R << 16 | G << 8 | B;
    }

}

package cz.wexder.pgrf.raster;


public interface Rastr {
    void drawPixel(int x, int y);
    void drawPixel(double x, double y, double c);
    void drawPixel(int x, int y, int color);
    public void setColor(int color);
    int getPixelColor(int x, int y);
}

package cz.wexder.pgrf;

import cz.wexder.pgrf.actionHandlers.KeyActionHandler;
import cz.wexder.pgrf.actionHandlers.MouseActionHandler;
import cz.wexder.pgrf.domain.PointDto;
import cz.wexder.pgrf.domain.Polygon;
import cz.wexder.pgrf.domain.RegularPolygon;
import cz.wexder.pgrf.filler.Filler;
import cz.wexder.pgrf.filler.SeedFillBuffer;
import cz.wexder.pgrf.raster.Rastr;
import cz.wexder.pgrf.raster.RastrBuffImg;
import cz.wexder.pgrf.renderer.LineRenderer;
import cz.wexder.pgrf.renderer.RendererBresenhamLine;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;

import static cz.wexder.pgrf.constants.Constants.*;

public class Main {

    private JPanel panel;
    private BufferedImage img;
    private Rastr rastr;
    private LineRenderer rend;
    private String rendererName = "Bresenham line algorithm";
    private int backgroundColor = 0x2f2f2f;
    private int drawColor = 0xff00ff;
    private int cutterColor = 0x00ff00;
    private int toBeFilledColor = 0xffff00;
    private int mode = LINE_MODE;
    private RegularPolygon regularPolygon;
    private List<Polygon> polygons = new LinkedList<>();
    private Polygon tempPolygon = null;
    private int tempPolygonColor = drawColor;
    private Polygon cutterPolygon = null;
    private Polygon toBeFilled = null;
    private Polygon current;
    private Filler filler;
    private boolean shiftPressed;

    public Main(int width, int height) {
        JFrame frame = new JFrame();

        frame.setLayout(new BorderLayout());
        frame.setTitle("UHK FIM PGRF : " + this.getClass().getName());
        frame.setResizable(false);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

        panel = new JPanel() {
            private static final long serialVersionUID = 1L;

            @Override
            public void paintComponent(Graphics g) {
                super.paintComponent(g);
                present(g);
            }
        };

        panel.setPreferredSize(new Dimension(width, height));

        frame.add(panel);
        frame.pack();
        frame.setVisible(true);
        MouseActionHandler mouseActionHandler = new MouseActionHandler(this);
        panel.addMouseListener(mouseActionHandler);
        panel.addMouseMotionListener(mouseActionHandler);
        frame.addKeyListener(new KeyActionHandler(this));
    }

    public void draw() {
        Graphics gr = img.getGraphics();
        gr.setColor(new Color(backgroundColor));
        gr.fillRect(0, 0, img.getWidth(), img.getHeight());
        if (filler != null) {
            filler.drawFilled();
        }
        if (polygons != null) {
            if (polygons.size() > 0) {
                for (Polygon pol : polygons) {
                    if (pol != null) {
                        pol.draw(rend, drawColor);
                    }
                }
            }
        }
        if (tempPolygon != null) {
            tempPolygon.draw(rend, tempPolygonColor);
        }
        if (cutterPolygon != null) {
            cutterPolygon.draw(rend, cutterColor);
        }
        if (toBeFilled != null) {
            toBeFilled.draw(rend, toBeFilledColor);
        }
        switch (mode) {
            case LINE_MODE:
                img.getGraphics().drawString("drawing: line", img.getWidth() - 300, 30);
                break;
            case LINE_CONTINUOUS_MODE:
                img.getGraphics().drawString("drawing: polygon", img.getWidth() - 300, 30);
                break;
            case REGULAR_POLYGON_MODE:
            case DRAWING_REGULAR_POLYGON_MODE:
                img.getGraphics().drawString("drawing: regular polygon", img.getWidth() - 300, 30);
                break;
            case SCAN_FILL_MODE:
                img.getGraphics().drawString("fill: scan fill", img.getWidth() - 300, 30);
                break;
            case SEED_FILL_MODE:
                img.getGraphics().drawString("fill: seed fill", img.getWidth() - 300, 30);
                break;
            case MOVE_MODE:
                img.getGraphics().drawString("polygon: move points", img.getWidth() - 300, 30);
                break;
        }
        img.getGraphics().drawString("renderer: " + rendererName, img.getWidth() - 300, 45);
        img.getGraphics().drawString("press 1 or 2 to change renderer", img.getWidth() - 300, 60);
        img.getGraphics().drawString("C to change between line and polygon", img.getWidth() - 300, 75);
        img.getGraphics().drawString("L to switch to regular polygon", img.getWidth() - 300, 90);
        img.getGraphics().drawString("R to clear all", img.getWidth() - 300, 105);
        panel.repaint();
    }

    public void present(Graphics graphics) {
        graphics.drawImage(img, 0, 0, null);
    }

    public void start() {
        draw();
        rastr = new RastrBuffImg(img);
        rend = new RendererBresenhamLine(rastr);
        filler = new SeedFillBuffer(rastr);
        panel.repaint();
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> new Main(WIDTH, HEIGHT).start());
    }


    // selectors and getters
    public BufferedImage getImg() {
        return img;
    }

    public void setImg(BufferedImage img) {
        this.img = img;
    }

    public int getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(int backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public RegularPolygon getRegularPolygon() {
        return regularPolygon;
    }

    public void setRegularPolygon(RegularPolygon regularPolygon) {
        this.regularPolygon = regularPolygon;
    }

    public Polygon getTempPolygon() {
        return tempPolygon;
    }

    public void setTempPolygon(Polygon tempPolygon) {
        this.tempPolygon = tempPolygon;
    }

    public Polygon getCurrent() {
        return current;
    }

    public void setCurrent(Polygon current) {
        this.current = current;
    }

    public List<Polygon> getPolygons() {
        return polygons;
    }

    public void setPolygons(List<Polygon> polygons) {
        this.polygons = polygons;
    }

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public Rastr getRastr() {
        return rastr;
    }

    public void setRastr(Rastr rastr) {
        this.rastr = rastr;
    }

    public JPanel getPanel() {
        return panel;
    }

    public void setPanel(JPanel panel) {
        this.panel = panel;
    }

    public LineRenderer getRend() {
        return rend;
    }

    public void setRend(LineRenderer rend) {
        this.rend = rend;
    }

    public String getRendererName() {
        return rendererName;
    }

    public void setRendererName(String rendererName) {
        this.rendererName = rendererName;
    }

    public Filler getFiller() {
        return filler;
    }

    public void setFiller(Filler filler) {
        this.filler = filler;
    }

    public int getDrawColor() {
        return drawColor;
    }

    public void setDrawColor(int drawColor) {
        this.drawColor = drawColor;
    }

    public Polygon getCutterPolygon() {
        return cutterPolygon;
    }

    public void setCutterPolygon(Polygon cutterPolygon) {
        this.cutterPolygon = cutterPolygon;
    }

    public int getTempPolygonColor() {
        return tempPolygonColor;
    }

    public void setTempPolygonColor(int tempPolygonColor) {
        this.tempPolygonColor = tempPolygonColor;
    }

    public int getCutterColor() {
        return cutterColor;
    }

    public void setCutterColor(int cutterColor) {
        this.cutterColor = cutterColor;
    }

    public Polygon getToBeFilled() {
        return toBeFilled;
    }

    public void setToBeFilled(Polygon toBeFilled) {
        this.toBeFilled = toBeFilled;
    }

    public boolean isShiftPressed() {
        return shiftPressed;
    }

    public void setShiftPressed(boolean shiftPressed) {
        this.shiftPressed = shiftPressed;
    }
}

package cz.wexder.pgrf.actionHandlers;

import cz.wexder.pgrf.Main;
import cz.wexder.pgrf.domain.PointDto;
import cz.wexder.pgrf.domain.Polygon;
import cz.wexder.pgrf.domain.RegularPolygon;
import cz.wexder.pgrf.filler.Filler;
import cz.wexder.pgrf.filler.ScanLineFiller;
import cz.wexder.pgrf.filler.SeedFillBuffer;
import cz.wexder.pgrf.filler.SeedFiller;
import cz.wexder.pgrf.renderer.LineRenderer;

import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import static cz.wexder.pgrf.constants.Constants.*;

public class MouseActionHandler implements MouseMotionListener, MouseListener {
    private static final int red = 0xff0000;
    private static final int white = 0xffffff;
    private static final int black = 0x0;

    Main main;
    int startX;
    int startY;
    int endX;
    int endY;
    Polygon hoverPolygon;
    PointDto hoverPoint;
    int fillColor = 0xff0000;
    int[][] fillPattern = new int[][]{
            {white, white, black, black, black, white, white, white, black, black, black, white, white},
            {white, black, red, red, red, black, white, black, red, red, red, black, white},
            {black, red, red, white, red, red, black, red, red, red, red, red, black},
            {black, red, white, red, red, red, red, red, red, red, red, red, black},
            {black, red, white, red, red, red, red, red, red, red, red, red, black},
            {black, red, red, red, red, red, red, red, red, red, red, red, black},
            {white, black, red, red, red, red, red, red, red, red, red, black, white},
            {white, white, black, red, red, red, red, red, red, red, black, white, white},
            {white, white, white, black, red, red, red, red, red, black, white, white, white},
            {white, white, white, white, black, red, red, red, black, white, white, white, white},
            {white, white, white, white, white, black, red, black, white, white, white, white, white},
            {white, white, white, white, white, white, black, white, white, white, white, white, white}
    };
    boolean pointAdded;

    public MouseActionHandler(Main main) {
        this.main = main;
    }

    private float difference(int x0, int y0, int x1, int y1) {
        return (float) Math.sqrt(Math.pow(x1 - x0, 2) + Math.pow(y1 - y0, 2));
    }

    private float angle(int x0, int y0, int x1, int y1) {
        int delta_x = x1 - x0;
        int delta_y = y1 - y0;
        return (float) Math.atan2(delta_y, delta_x);
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if (e.getButton() == MouseEvent.BUTTON1) {
            switch (main.getMode()) {
                case LINE_MODE:
                case REGULAR_POLYGON_MODE:
                    startX = e.getX();
                    startY = e.getY();
                    main.setCurrent(new Polygon());
                    main.getCurrent().addPoint(new PointDto(startX, startY));
                    break;
                case LINE_CONTINUOUS_MODE:
                    if (main.getCurrent() == null) {
                        main.setCurrent(new Polygon());
                        main.getPolygons().add(main.getCurrent());
                    }
                    if (main.getCurrent().getPoints().size() == 0) {
                        startX = e.getX();
                        startY = e.getY();
                        main.getCurrent().addPoint(new PointDto(startX, startY));
                    }
                    break;
                case SEED_FILL_MODE:
                    try {
                        Filler filler = main.getFiller();
                        if (filler instanceof SeedFillBuffer) {
                            ((SeedFillBuffer) filler).setPoint(new PointDto(e.getX(), e.getY()));
                        }
                        if (filler instanceof SeedFiller) {
                            ((SeedFiller) filler).setPoint(new PointDto(e.getX(), e.getY()));
                        }
                        filler.fill(fillPattern);
                    } catch (StackOverflowError error) {
                        System.out.println(error.toString());
                    }
                    main.draw();
                    break;
                case SCAN_FILL_MODE:
                    Filler filler = main.getFiller();
                    if (main.getToBeFilled() != null) {
                        Polygon tempPolygon = main.getToBeFilled();
                        ((ScanLineFiller) filler).setPolygon(tempPolygon);
                        main.getFiller().fill(fillColor);
                        main.setTempPolygon(null);
                        main.draw();
                    }
                    break;
            }
        }
        if (e.getButton() == MouseEvent.BUTTON3) {
            if (main.getMode() == MOVE_MODE) {
                if (hoverPolygon != null && hoverPoint != null) {
                    if (hoverPolygon.getPoints().size() > 3) {
                        Filler filler = main.getFiller();
                        filler.clearFilled();
                        hoverPolygon.getPoints().remove(hoverPoint);
                        main.setTempPolygon(null);
                        if (main.getToBeFilled() != null) {
                            Polygon tempPolygon = main.getToBeFilled();
                            ((ScanLineFiller) filler).setPolygon(tempPolygon);
                        }
                        filler.fill(fillColor);
                        main.draw();
                    }
                }
            }
        }
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        // e.getButton() == 0 cause weird implementation in Windows JVM
        if (e.getButton() == MouseEvent.BUTTON1 || e.getButton() == 0) {
            main.draw();
            if (main.getMode() == REGULAR_POLYGON_MODE || main.getMode() == LINE_CONTINUOUS_MODE || main.getMode() == LINE_MODE) {
                Polygon current = main.getCurrent();
                LineRenderer rend = main.getRend();
                endX = e.getX();
                endY = e.getY();
                rend.drawLine(startX, startY, endX, endY, Color.RED.getRGB());
                rend.drawLine(current.getPoints().get(0).getX(), current.getPoints().get(0).getY(), endX, endY, Color.RED.getRGB());
            } else if (main.getMode() == MOVE_MODE) {
                if (hoverPoint != null) {
                    Filler filler = main.getFiller();
                    filler.clearFilled();
                    main.draw();
                    main.setTempPolygon(null);
                    if (main.isShiftPressed() && !pointAdded) {
                        if (hoverPolygon != null) {
                            int i = hoverPolygon.getPoints().indexOf(hoverPoint);
                            hoverPolygon.getPoints().add(i + 1, new PointDto(hoverPoint.getX(), hoverPoint.getY()));
                            pointAdded = true;
                        }
                    } else {
                        hoverPoint.setX(e.getX());
                        hoverPoint.setY(e.getY());
                    }
                    if (main.getToBeFilled() != null && main.getPolygons().get(0) != null) {
                        main.setToBeFilled(main.getPolygons().get(0).cut(main.getCutterPolygon()));
                    }
                    if (main.getToBeFilled() != null && filler instanceof ScanLineFiller) {
                        Polygon tempPolygon = main.getToBeFilled();
                        ((ScanLineFiller) filler).setPolygon(tempPolygon);
                    }
                    filler.fill(fillColor);
                }
            }
            main.draw();
        }
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        int tempX = e.getX();
        int tempY = e.getY();
        if (main.getMode() == DRAWING_REGULAR_POLYGON_MODE) {
            main.draw();
            Polygon current = main.getCurrent();
            LineRenderer rend = main.getRend();
            rend.drawLine(current.getPoints().get(0).getX(), current.getPoints().get(0).getY(), endX, endY, Color.RED.getRGB());
            main.getRegularPolygon().draw(new PointDto(startX, startY), angle(startX, startY, endX, endY), difference(startX, startY, endX, endY), (int) difference(tempX, tempY, endX, endY));
            main.setTempPolygon(main.getRegularPolygon().getPolygon());
        } else if (main.getMode() == MOVE_MODE) {
            main.setTempPolygon(null);
            hoverPoint = null;
            if (main.getPolygons().size() > 0) {
                main.getPolygons().forEach(polygonDto -> {
                    if (polygonDto != null) {
                        polygonDto.getPoints().forEach(pointDto -> checkHover(pointDto, polygonDto, new PointDto(tempX, tempY), main.getDrawColor()));
                    }
                });
            }
            if (main.getCutterPolygon() != null) {
                main.getCutterPolygon().getPoints().forEach(pointDto -> checkHover(pointDto, main.getCutterPolygon(), new PointDto(tempX, tempY), main.getCutterColor()));
            }
            if (main.getToBeFilled() != null && main.getPolygons().get(0) != null) {
                main.setToBeFilled(main.getPolygons().get(0).cut(main.getCutterPolygon()));
            }
            main.draw();
        }
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if (e.getButton() == MouseEvent.BUTTON1) {
            pointAdded = false;
            if (main.getMode() != SEED_FILL_MODE && main.getMode() != SCAN_FILL_MODE && main.getMode() != MOVE_MODE) {
                if (main.getMode() == LINE_MODE || main.getMode() == LINE_CONTINUOUS_MODE) {
                    LineRenderer rend = main.getRend();
                    Polygon current = main.getCurrent();
                    if (main.getMode() == LINE_CONTINUOUS_MODE) {
                        main.draw();
                        endX = e.getX();
                        endY = e.getY();
                        rend.drawLine(startX, startY, endX, endY, Color.RED.getRGB());
                        current.addPoint(new PointDto(endX, endY));
                        startX = e.getX();
                        startY = e.getY();
                        main.draw();
                    } else {
                        current.addPoint(new PointDto(endX, endY));
                        main.getPolygons().add(current);
                        main.setCurrent(new Polygon());
                        main.draw();
                    }
                } else {
                    if (main.getMode() != DRAWING_REGULAR_POLYGON_MODE) {
                        main.draw();
                        endX = e.getX();
                        endY = e.getY();
                        RegularPolygon regularPolygon = new RegularPolygon(new PointDto(startX, startY), angle(startX, startY, endX, endY), difference(startX, startY, endX, endY), 3);
                        main.setRegularPolygon(regularPolygon);
                        main.setTempPolygon(regularPolygon.getPolygon());
                        main.draw();
                        main.setMode(DRAWING_REGULAR_POLYGON_MODE);
                    } else {
                        main.getPolygons().add(main.getTempPolygon());
                        main.setTempPolygon(null);
                        main.setMode(REGULAR_POLYGON_MODE);
                        main.draw();
                    }

                }
            }
        }
    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }


    private void checkHover(PointDto pointDto, Polygon polygonDto, PointDto mousePos, int color) {
        if (difference(pointDto.getX(), pointDto.getY(), mousePos.getX(), mousePos.getY()) < 20) {
            main.setRegularPolygon(new RegularPolygon(new PointDto(pointDto.getX(), pointDto.getY()), angle(startX, startY, endX, endY), 20, 30));
            main.setTempPolygon(main.getRegularPolygon().getPolygon());
            hoverPoint = pointDto;
            hoverPolygon = polygonDto;
            main.setTempPolygonColor(color);
        }
    }
}

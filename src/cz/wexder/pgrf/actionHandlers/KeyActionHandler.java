package cz.wexder.pgrf.actionHandlers;

import cz.wexder.pgrf.Main;
import cz.wexder.pgrf.domain.PointDto;
import cz.wexder.pgrf.domain.Polygon;
import cz.wexder.pgrf.domain.RegularPolygon;
import cz.wexder.pgrf.filler.ScanLineFiller;
import cz.wexder.pgrf.filler.SeedFillBuffer;
import cz.wexder.pgrf.renderer.RendererBresenhamLine;
import cz.wexder.pgrf.renderer.RendererWuLine;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.Random;

import static cz.wexder.pgrf.constants.Constants.*;

public class KeyActionHandler implements KeyListener {
    private Main main;
    Random randomGen = new Random();


    public KeyActionHandler(Main main) {
        this.main = main;
    }

    @Override
    public void keyTyped(KeyEvent keyEvent) {
        char keyCode = keyEvent.getKeyChar();
        switch (keyCode) {
            case '1':
                main.setRend(new RendererBresenhamLine(main.getRastr()));
                main.setRendererName("Bresenham line algorithm");
                break;
            case '2':
                main.setRend(new RendererWuLine(main.getRastr()));
                main.setRendererName("Wu line algorithm");
                break;
            case 'c':
                if (main.getMode() == LINE_MODE) {
                    main.setMode(LINE_CONTINUOUS_MODE);
                } else {
                    main.setMode(LINE_MODE);
                }
                main.setCurrent(new Polygon());
                main.getPolygons().add(main.getCurrent());
                break;
            case 'l':
                main.getPolygons().add(main.getCurrent());
                main.setCurrent(new Polygon());
                main.setMode(REGULAR_POLYGON_MODE);
                break;
            case 'r':
                main.getPolygons().clear();
                main.getFiller().clearFilled();
                main.setToBeFilled(null);
                main.setCutterPolygon(null);
                break;
            case 'f':
                main.setMode(SEED_FILL_MODE);
                main.setFiller(new SeedFillBuffer(main.getRastr()));
                break;
            case 's':
                main.setMode(SCAN_FILL_MODE);
                Polygon polygon;
                if (main.getPolygons().size() != 1) {
                    main.getPolygons().clear();
                    polygon = new RegularPolygon(new PointDto((WIDTH / 2 + random(50, -50)), (HEIGHT / 2 + random(30, -30))), (float) Math.random(), random(120, 30), 5).getPolygon();
                    main.getPolygons().add(polygon);
                } else {
                    polygon = main.getPolygons().get(0);
                }
                main.getFiller().clearFilled();
                main.setFiller(new ScanLineFiller(main.getRastr()));
                Polygon cutterPolygon;
                if (main.getCutterPolygon() == null) {
                    cutterPolygon = new RegularPolygon(new PointDto((WIDTH / 2 + random(50, -50)), (HEIGHT / 2 + random(30, -30))), (float) Math.random(), random(120, 30), 5).getPolygon();
                    main.setCutterPolygon(cutterPolygon);
                } else {
                    cutterPolygon = main.getCutterPolygon();
                }
                main.setToBeFilled(polygon.cut(cutterPolygon));
                break;
            case 'w':
                main.setMode(MOVE_MODE);
                break;
        }
        main.draw();
    }

    @Override
    public void keyPressed(KeyEvent keyEvent) {
       if(keyEvent.getExtendedKeyCode() == 16){
           main.setShiftPressed(true);
       }
    }

    @Override
    public void keyReleased(KeyEvent keyEvent) {
        if(keyEvent.getExtendedKeyCode() == 16){
            main.setShiftPressed(false);
        }
    }

    private int random(int max, int min) {
        return randomGen.nextInt(max - min) + min;
    }
}
